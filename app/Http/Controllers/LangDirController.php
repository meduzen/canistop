<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Punic\Misc as Punic;

class LangDirController extends Controller
{
    public function __invoke(string $lang): JsonResponse
    {
        $direction = Punic::getCharacterOrder($lang) == 'right-to-left'
            ? 'rtl'
            : 'ltr';

        return response()->json($direction);
    }
}
