<?php

namespace App\Services\Import\GitHub;

use Illuminate\Support\Facades\{File, Http, Storage};
use Illuminate\Support\Str;

use ZipArchive;

class GitHubImportService
{
    protected $timeout = 120;

    public function __construct(

        // The storage folder where repositories are stored once fetched.
        public readonly string $storage,

        // The storage folder where repository data are unzipped.
        public readonly string $destination,

        // The intermediate storage folder where repository are unzipped.
        public readonly string $temp,
    ) {}

    /**
     * Download .zip repository.
     */
    public function fetch(string $repo): bool
    {
        $res = Http::timeout($this->timeout)
            ->get("https://api.github.com/repos/{$repo}/zipball");

        if (!$res->ok()) {
            return false;
        }

        $file_name = str_replace('attachment; filename=', '', $res->header('Content-Disposition'));

        return Storage::put("{$this->storage}/{$file_name}", $res->body());
    }

    /**
     * Unzip data from repository archive.
     */
    public function extract(string $repo, string $folder_to_extract = null, string $file_ext = null): bool
    {
        // Open archive

        $path = $this->getRepoPath($repo);

        $zip = new ZipArchive;
        if ($zip->open(storage_path("app/{$path}")) !== true) {
            return false;
        }

        // Get files list.

        $folder = $zip->getNameIndex(0) . $folder_to_extract;
        $files_list = $this->getContentList($zip, $folder, $file_ext);

        // Extract files and move them.

        Storage::deleteDirectory($this->temp);

        $extraction_done = $zip->extractTo(storage_path("app/{$this->temp}"), $files_list);
        $zip->close();

        File::moveDirectory(
            storage_path("app/{$this->temp}/{$folder}"),
            storage_path("app/{$this->destination}/{$folder_to_extract}"),
            true,
        );

        Storage::deleteDirectory($this->temp);

        return $extraction_done;
    }

    /**
     * Get repository path from a list of repositories.
     */
    protected function getRepoPath(string $repo): string
    {
        // kebab-cased repo name from `owner/name`
        $slug = Str::of($repo)->explode('/')->implode('-');

        return collect(Storage::files($this->storage))
            ->filter(fn ($file) => Str::endsWith($file, '.zip'))
            ->first(fn ($path) => Str::contains($path, $slug));
    }

    /**
     * Get content of a zipped directory.
     */
    protected function getContentList(ZipArchive $zip, string $folder, string $file_ext = null): array
    {
        $paths = [];

        $index = $zip->locateName($folder . '/') + 1;

        while ($index < $zip->numFiles) {
            $path = $zip->getNameIndex($index);

            if (!Str::contains($path, $folder)) {
                return $paths;
            }

            if (!$file_ext || Str::endsWith($path, $file_ext)) {
                $paths[] = $path;
            }

            $index++;
        }

        return $paths;
    }
}
