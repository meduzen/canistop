<?php

namespace App\Services\Import;

use App\Services\Import\GitHub\GitHubImportService;
use Illuminate\Support\Facades\Storage;

class DataFetchService
{
    /**
     * The storage folder where repositories are stored once fetched.
     */
    protected string $reposFolder = 'repositories';

    /**
     * The storage folder where repository data are unzipped.
     */
    protected string $destination = 'data';

    /**
     * The intermediate storage folder where repository are unzipped.
     */
    protected string $temp = 'temp';

    protected GitHubImportService $github;


    public function __construct()
    {
        $this->github = new GitHubImportService(
            $this->reposFolder,
            $this->destination,
            $this->temp,
        );
    }

    /**
     * Download repositories.
     */
    public function fetch() :bool
    {
        Storage::deleteDirectory($this->reposFolder);
        Storage::makeDirectory($this->reposFolder);

        return
            $this->github->fetch('Fyrd/caniuse')
            && $this->github->fetch('mdn/browser-compat-data');
    }

    /**
     * Unzip data.
     */
    public function extract(): bool
    {
        Storage::makeDirectory($this->destination);

        return
            $this->github->extract('Fyrd/caniuse', 'region-usage-json', 'json')
            && $this->github->extract('mdn/browser-compat-data', 'browsers', 'json');
    }

    /**
     * Clear pulled data.
     */
    public function clear() :void
    {
        Storage::deleteDirectory($this->destination);
        Storage::deleteDirectory($this->reposFolder);
    }
}
