<?php

namespace App\Services\Import;

use App\Notifications\DataUpdated;
use Carbon\Carbon;
use Illuminate\Support\Facades\{Notification, Storage};
use Illuminate\Support\{Arr, Collection, Str, Stringable};

class DataMakerService
{
    protected Collection $regionsFiles;
    protected string $regionsStorage = 'data/region-usage-json';

    protected Collection $browsersFiles;
    protected string $browsersStorage = 'data/browsers';

    protected array $regions = [];
    protected array $regions_codes = [];
    protected Collection $browsers;
    protected Collection $mdnBrowsers;

    protected string $metadataStorage = 'public/metadata.json';
    protected string $metadataLightStorage = 'public/metadata-light.json';

    protected ?string $previousDate;

    public function __construct()
    {
        $this->regionsFiles = $this->getDataFiles($this->regionsStorage);
        $this->browsersFiles = $this->getDataFiles($this->browsersStorage);

        $this->previousDate = $this->getDate();
    }

    /**
     * Create data files.
     */
    public function create(): bool
    {
        // Compute.
        $this->compute();

        if (empty($this->browsers)) {
            return false;
        }

        $this->computeMdnBrowsersData();

        // Write files.
        if (!$this->storeMetadata() || !$this->storeData()) {
            return false;
        }

        // Notify
        if ($this->date !== $this->previousDate) {
            $this->notify();
        }

        return true;
    }

    /**
     * Compute date, regions and browsers.
     */
    protected function compute(): void
    {
        $this->regionsFiles->each(function($filename) {
            $file = json_decode(Storage::get($filename));
            $region_code = (string) $this->getRegionCode($filename);

            // Date
            $this->date = Carbon::create($file->month)->format('F Y');

            // Legacy file: region code => region name
            $this->regions[$region_code] = $file->name;

            // Light file without region name (browser `Intl.DisplayNames`).
            if (strlen($region_code) == '2') {
                // region code
                $this->regions_codes[] = $region_code;
            } else {
                /**
                 * For alt-regions, we keep their names:
                 * [
                 *  code => region code,
                 *  name => region name,
                 * ]
                 */
                $this->regions_codes[] = [
                    'code' => $region_code,
                    'name' => $file->name,
                ];
            }

            // Browsers from country->browser->version to browser->version->country.
            if (empty($this->browsers)) {
                $this->browsers = collect($file->data);
                $this->browsers = $this->browsers->map(function($versions, $browser) use($region_code) {
                    return collect($versions)->map(function($usage, $version) use($region_code) {
                        $region_usage = !is_null($usage) ? $usage : 0;
                        $usage = [];
                        $usage[$region_code] = $region_usage;
                        return $usage;
                    });
                });
            } else {
                $region_data = collect($file->data);
                $this->browsers = $this->browsers->map(function($versions, $browser) use($region_code, $region_data) {
                    return collect($versions)->map(
                        function($usage, $version) use($region_code, $region_data, $browser) {
                            $usage[$region_code] = $region_data[$browser]->$version ?? 0;
                            return $usage;
                        }
                    );
                });
            }
        });
    }

    protected function computeMdnBrowsersData()
    {
        $this->mdnBrowsers = collect();

        $this->browsersFiles->each(function($filename) {
            $browser_key = (string) $this->getBrowserKey($filename);

            $file = collect(json_decode(Storage::get($filename))->browsers);

            $this->mdnBrowsers->put($browser_key, $file[$browser_key]);
        });
    }

    /**
     * Get data file date.
     */
    protected function getDate(): ?string
    {
        if (!Storage::exists($this->metadataStorage)) {
            return null;
        }

        return json_decode(Storage::get($this->metadataStorage))->date;
    }

    /**
     * Notify about fresh data.
     */
    protected function notify(): void
    {
        Notification::route('osef', config('services.slack.webhook_url'))
            ->notify(new DataUpdated($this->date));
    }

    /**
     * Return data files extracted from repository.
     */
    protected function getDataFiles($path): Collection
    {
        return collect(Storage::files($path))
            ->filter(fn($file) => Str::endsWith($file, '.json'));
    }

    /**
     * Return region code from filename.
     */
    protected function getRegionCode($filename): Stringable
    {
        // 'data/region-usage-json/REGION_CODE.json'
        return Str::of($filename)->afterLast('/')->before('.json')->lower();
    }

    /**
     * Return browser key from filename.
     */
    protected function getBrowserKey($filename): Stringable
    {
        // 'data/browsers/BROWSER_KEY.json'
        return Str::of($filename)->afterLast('/')->before('.json')->lower();
    }

    /**
     * Store metadata.
     */
    protected function storeMetadata(): bool
    {
        $browsers = $this->browsers->map(function($browser, $name) {
            $config = config('canistop.browsers');

            if (
                Arr::has($config[$name], 'mdn')
                && $this->mdnBrowsers->has($config[$name]['mdn'])
            ) {

                // Add browser versions details from MDN data.

                $mdn_name = $config[$name]['mdn'];
                $versions = [];

                $versions = collect($browser)->keys()->mapWithKeys(function($key) use ($versions, $mdn_name) {
                    if (!property_exists($this->mdnBrowsers[$mdn_name]->releases, $key)) {
                        return [$key =>  null];
                    }

                    return [
                        $key => collect($this->mdnBrowsers[$mdn_name]->releases->{$key})
                            ->only(['status', 'release_date'])
                            ->put('version', $key),
                    ];
                });
            } else {

                // No browser versions details available.

                $versions = collect($browser)->keys();
            }

            return array_merge($config[$name], ['versions' => $versions]);
        });

        $v1_file_saved = Storage::put($this->metadataStorage, json_encode([
            'date' => $this->date,
            'browsers' => $browsers,
            'regions' => $this->regions,
        ]));

        if ($v1_file_saved) {
            return Storage::put($this->metadataLightStorage, json_encode([
                'date' => $this->date,
                'browsers' => $browsers,
                'regions_codes' => $this->regions_codes,
            ]));
        }
    }

    /**
     * Store data.
     */
    protected function storeData(): bool
    {
        $are_stored = true;

        $this->browsers->each(function($browser, $name) use($are_stored) {
            if (!$are_stored) { return false; }

            collect($browser)->each(function($data, $version) use($name, $are_stored) {
                $res = Storage::put('public/data/' . $name . '-' . $version . '.json', json_encode([
                    'date' => $this->date,
                    'browser' => $name,
                    'version' => $version,
                    'usage' => $data,
                ]));
                if (!$res) {
                    $are_stored = false;
                    return false;
                }
            });
        });

        return $are_stored;
    }
}
