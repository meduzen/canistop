<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

use Revolution\Laravel\Notification\Mastodon\{MastodonChannel, MastodonMessage};

class DataUpdated extends Notification
{
    use Queueable;

    protected string $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($month)
    {
        $this->message = "Browsers data usage for {$month} are now available on https://canistop.net";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     */
    public function via($notifiable): array
    {
        return [
            MastodonChannel::class,
        ];
    }

    /**
     * Get the Mastodon status representation of the notification.
     *
     * @param  mixed  $notifiable
     */
    public function toMastodon($notifiable): MastodonMessage
    {
        return MastodonMessage::create($this->message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
