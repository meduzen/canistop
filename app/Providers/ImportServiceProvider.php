<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Import\ImportService;

class ImportServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ImportService::class, fn () => new ImportService());
    }
}
