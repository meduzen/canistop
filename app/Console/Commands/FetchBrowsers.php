<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\Import\DataFetchService;

class FetchBrowsers extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'browsers:fetch';

    /**
     * The console command description.
     */
    protected $description = 'Get browsers market shares from repository';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(DataFetchService $import)
    {
        // Fetch

        $this->line('Fetching browsers data.');

        if (!$import->fetch()) {
            return $this->error('Error while fetching data.');
        }

        $this->info('Data fetched!');

        // Extract

        $this->line('Extracting regions data.');

        if (!$import->extract()) {
            return $this->error('Error while fetching data.');
        }

        $this->info('Data extracted!');
    }
}
