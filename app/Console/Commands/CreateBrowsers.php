<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\Import\{DataFetchService, DataMakerService};

class CreateBrowsers extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'browsers:create';

    /**
     * The console command description.
     */
    protected $description = 'Create browsers files from fetched data';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(DataMakerService $maker, DataFetchService $import)
    {
        $this->line('Updating market shares.');

        if (!$maker->create()) {
            return $this->error('Error while trying to create data.');
        }

        $this->info('Market shares updated!');

        $import->clear();
    }
}
