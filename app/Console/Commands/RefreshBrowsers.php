<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RefreshBrowsers extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'browsers:refresh';

    /**
     * The console command description.
     */
    protected $description = 'Wipe and refresh all browsers market shares';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('browsers:fetch');
        $this->call('browsers:create');
    }
}
