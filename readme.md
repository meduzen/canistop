# Can I Stop

A tool helping you to drop Internet Explorer 11 support before Microsoft [officially does](https://support.microsoft.com/en-us/help/17454/lifecycle-faq-internet-explorer).

### Installation

- copy `.env.example` to `.env` and fill it
- `composer install`
- `php artisan storage:link`
- `php artisan browsers:refresh`
- `npm install`

#### Laravel Telescope

If you want to use Laravel Telescope, follow [its installation guide](https://laravel.com/docs/5.8/telescope#installation).

### Run

- `php artisan serve` or your favorite tool (like [Laravel Valet](https://laravel.com/docs/master/valet) or [Laravel Homestead](https://laravel.com/docs/master/homestead))
- `npm run dev|watch|prod|sw|sw-watch|full`

Notes:
- `npm run prod` doesn’t compile the Service Worker;
- `npm run sw` only compiles the Service Worker;
- `npm run full` compiles all the things.

### Watch

- `npm run serve` doesn’t watch the Service Worker changes;
- `npm run sw-watch` only watches the Service Worker changes;
- **To watch both the app and Service Worker changes**, run two terminal tabs: one with `npm run serve` and one with `npm run sw-watch`.

## Data update

`php artisan list browsers` to display commands documentation.

1. `php artisan browsers:fetch`: get browsers market shares from repository.
2. `php artisan browsers:create`: create browsers file from fetched data.

Or `php artisan browsers:refresh` to do both in a row.

## End of support dates by Microsoft

From the [Microsoft Lifecycle search page] (https://support.microsoft.com/en-us/lifecycle/search)

- Windows 7 Service Pack 1: [14th January 2020](https://support.microsoft.com/en-us/lifecycle/search?alpha=Windows%207%20Service%20Pack%201)
- Windows 8: [10th January 2023](https://support.microsoft.com/en-us/lifecycle/search?alpha=Windows%208.1)
- Windows 10: [14th October 2025](https://support.microsoft.com/en-us/lifecycle/search?alpha=Windows%2010)

## Learning Laravel and Vue.JS

- [Laravel](https://laravel.com/docs)
- [Vue JS](https://vuejs.org)
- [Both](https://laracasts.com)

## To add to this file

- Contributing
- License
