<?php

return [
    'browsers' => [
        'chrome' => [
            'name' => 'Chrome',
            'minor' => false,
            'type' => 'desktop',
            'mdn' => 'chrome',
        ],
        'firefox' => [
            'name' => 'Firefox',
            'minor' => false,
            'type' => 'desktop',
            'mdn' => 'firefox',
        ],
        'opera' => [
            'name' => 'Opera',
            'minor' => true,
            'type' => 'desktop',
            'mdn' => 'opera',
        ],
        'safari' => [
            'name' => 'Safari',
            'minor' => false,
            'type' => 'desktop',
            'mdn' => 'safari',
        ],
        'ios_saf' => [
            'name' => 'Safari iOS',
            'minor' => false,
            'type' => 'mobile',
            'mdn' => 'safari_ios',
        ],
        'android' => [
            'name' => 'Android Browser',
            'minor' => true,
            'type' => 'mobile',
        ],
        'edge' => [
            'name' => 'Edge',
            'minor' => false,
            'type' => 'desktop',
            'mdn' => 'edge',
        ],
        'op_mob' => [
            'name' => 'Opera Mobile',
            'minor' => true,
            'type' => 'mobile',
            'mdn' => 'opera_android',
        ],
        'ie' => [
            'name' => 'Internet Explorer',
            'minor' => false,
            'type' => 'desktop',
            'mdn' => 'ie',
        ],
        'samsung' => [
            'name' => 'Samsung Internet',
            'minor' => false,
            'type' => 'mobile',
            'mdn' => 'samsunginternet_android',
        ],
        'ie_mob' => [
            'name' => 'IE Mobile',
            'minor' => true,
            'type' => 'mobile',
        ],
        'bb' => [
            'name' => 'Black Berry',
            'minor' => true,
            'type' => 'mobile',
        ],
        'and_chr' => [
            'name' => 'Chrome Android',
            'minor' => false,
            'type' => 'mobile',
            'mdn' => 'chrome_android',
        ],
        'baidu' => [
            'name' => 'Baidu',
            'minor' => true,
            'type' => 'mobile',
            'mdn' => 'baidu', //
        ],
        'and_ff' => [
            'name' => 'Firefox Android',
            'minor' => true,
            'type' => 'mobile',
            'mdn' => 'firefox_android',
        ],
        'and_uc' => [
            'name' => 'UC Browser',
            'minor' => true,
            'type' => 'mobile',
            'mdn' => 'uc_android',
        ],
        'kaios' => [
            'name' => 'KaiOS',
            'minor' => true,
            'type' => 'mobile',
        ],
        'and_qq' => [
            'name' => 'QQ Browser',
            'minor' => true,
            'type' => 'mobile',
            'mdn' => 'qq_android',
        ],
        'op_mini' => [
            'name' => 'Opera Mini',
            'minor' => true,
            'type' => 'mobile',
        ],
    ],
];
