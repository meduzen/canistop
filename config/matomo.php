<?php

return [
    'server' => env('MATOMO_SERVER', null),
    'site_id' => env('MATOMO_ID', null),
];
