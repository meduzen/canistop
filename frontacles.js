// env
const env = require('dotenv').config().parsed
const isProd = process.env.NODE_ENV === 'production'
const mode = isProd ? 'production' : 'development'

// path
const path = require('path')
const thePath = (folder = '') => path.resolve(__dirname, folder)
const assets = 'resources'

// plugins: folder cleaning, versioning
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const { WebpackManifestPlugin: ManifestPlugin } = require('webpack-manifest-plugin')

// plugins: reload & cli output
const FriendlyErrorsPlugin = require('@soda/friendly-errors-webpack-plugin')
const NotifierPlugin = require('webpack-build-notifier')

// plugins: CSS & JS
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const { VueLoaderPlugin } = require("vue-loader");
const EsLintPlugin = require('eslint-webpack-plugin')

// Cleaning options
const cleanPluginOptions = {
  dangerouslyAllowCleanPatternsOutsideProject: true,
}

// mix-manifest.json
const manifestPluginOptions = {
  fileName: thePath('public/mix-manifest.json'),
  seed: {},
  filter: entry =>
    (
      entry.isInitial // files specified as `entry` points in Webpack configs
      || entry.isChunk // (JS) include dynamically imported files
    )
    && !entry.path.split('?')[0].endsWith('.map'), // exclude sourceMaps
}

// Notifications options
const notifierPluginOptions = {
  logo: thePath('resources/app/android-chrome-192x192.png'),
  formatSuccess: () => 'Death to IE11!',
  sound: false,
  notifyOptions: { timeout: 4 },

  // Errors/warnings format. Example: “3 errors – resources/sass/oh-no.scss”
  messageFormatter: (error, filepath, status, errorCount) => `
    ${errorCount} ${status}${errorCount === 1 ? '' : 's'} – ${filepath.replace(thePath() + '/', '')}`,
}

// Significantly increase startup time after first run.
const webpackCacheOptions = {
  type: 'filesystem',
}

/**
 * Use the empty object until this issue is fixed:
 * https://github.com/babel/babel-loader/issues/690
 * In other words, the babel cache is currently disabled, which slows a bit the
 * build process. A PR for this is well advanced:
 * https://github.com/babel/babel/pull/11741
 *
 * Reason: when the list of supported browsers changes (browserslist), the
 * cache doesn’t get cleared, which may lead to broken code that should
 * work (or stop to work) while testing on various browsers/devices.
 */
// const babelLoaderOptions = {};
const babelLoaderOptions = {
  cacheCompression: false,
  cacheDirectory: !isProd,
}

// ESLint options
const esLintPluginOptions = {
  cache: true, // cache is cleaned on `npm install`
  cacheStrategy: 'content',
  extensions: ['js', 'vue'],
  fix: env.ES_LINT_AUTOFIX == 'true',
  formatter: env.ES_LINT_FORMATTER ?? 'stylish',
}

/*********/
/* 1. JS */
/*********/

const configJs = {

  entry: {
    app: `./${assets}/js/app.js`,
  },

  output: {
    filename: '[name].js?id=[contenthash]',
    path: thePath('public/js'),
    publicPath: '/js/',
  },

  target: 'browserslist',

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: babelLoaderOptions,
      },
      {
        test: /\.vue$/,
        include: thePath(assets + '/js'),
        loader: 'vue-loader',
      },
    ]
  },

  cache: isProd ? false : webpackCacheOptions,

  optimization: {
    minimize: isProd,

    /**
     * For now, we’ll don’t let Webpack automatically split chunks
     * (https://webpack.js.org/plugins/split-chunks-plugin/#defaults).
     *
     * Instead, dynamic import is used. We’ll see later if should be
     * reconsidered or not. See also:
     * https://blog.logrocket.com/guide-performance-optimization-webpack/
     */
    splitChunks: false,

    removeAvailableModules: isProd,
  },

  plugins: [
    new CleanWebpackPlugin(cleanPluginOptions),
    ...(isProd ? [] : [new EsLintPlugin(esLintPluginOptions)]),
    new ManifestPlugin({ basePath: '/js/', ...manifestPluginOptions }),
    new FriendlyErrorsPlugin(),
    new NotifierPlugin({ title: 'JS', ...notifierPluginOptions }),

    new VueLoaderPlugin(),
  ],

  mode,

  devtool: isProd ? 'source-map' : 'eval-cheap-source-map',

  performance: {
    hints: false,
  },

  stats: {
    modules: false,
    version: false,
    // excludeAssets: [
    //   /.*\.(ico|jpg|png|svg|webmanifest|xml)$/, // Web Manifest and icons
    //   /.*\.map$/, // Sourcemaps
    // ],
  },
}

/*********/
/* 2. CSS */
/*********/

const configCSS = {

  entry: {
    app: `./${assets}/sass/app.scss`,
  },

  output: {
    path: thePath('public/css'),
    publicPath: '/css/', // currently required (https://github.com/shellscape/webpack-manifest-plugin/issues/229)
  },

  module: {
    rules: [
      {
        test: /\.scss$/,
        include: thePath(`${assets}/sass`),
        use: [
          MiniCssExtractPlugin.loader,
          { loader: 'css-loader', options: { importLoaders: 2, url: false, sourceMap: true } },
          { loader: 'postcss-loader', options: { sourceMap: true } },
          { loader: 'sass-loader', options: {
            implementation: require('node-sass'),
            sassOptions: { outputStyle: 'expanded' },
            sourceMap: true
          }},
        ],
      },
      // {
      //   test: /\.(jpg|png)$/,
      //   include: thePath(`${assets}/img`),
      //   type: 'asset/resource',
      // },
    ]
  },

  plugins: [
    new MiniCssExtractPlugin({ filename: '[name].css?id=[fullhash]' }),
    new CleanWebpackPlugin(cleanPluginOptions),
    new ManifestPlugin({ basePath: '/css/', ...manifestPluginOptions }),
    new FriendlyErrorsPlugin(),
    new NotifierPlugin({ title: 'CSS', ...notifierPluginOptions }),
  ],

  mode,

  devtool: isProd ? 'source-map' : 'cheap-module-source-map',

  performance: {
    hints: false,
  },

  // What is displayed by the CLI (https://webpack.js.org/configuration/stats)
  stats: {
    entrypoints: false,
    groupAssetsByEmitStatus: false,
    excludeAssets: [/.*\.(?!(css|js))/], // only show stats for JS and CSS files
    publicPath: true,
    hash: false,
    performance: false,
    modules: false,
    version: false,
  },
}

/******************/
/* 3. Other files */
/******************/

// Other files without entry point, so we push them to the previous config.
configCSS.plugins.push(
  new CopyPlugin({ patterns: [
    { from: `${assets}/app/`, to: thePath('public') },
  ]}),
)

/************************/
/* 4. Local development */
/************************/

/**
 * HTTPS for webpack-dev-server with Laravel Valet (macOS users)
 * Laravel Valet HTTPS: https://laravel.com/docs/5.7/valet#securing-sites
 */

let devServerHttps = false
let serverProtocol = 'http'

if (
  env.VALET_HTTPS === 'true'
  && typeof env.VALET_USER === 'string'
  && env.VALET_CERTIFICATES_PATH
) {
  let certificatesPath = `/Users/${env.VALET_USER}/${env.VALET_CERTIFICATES_PATH}/${env.LOCAL_URL.substring(8)}`

  devServerHttps = {
    key: `${certificatesPath}.key`,
    cert: `${certificatesPath}.crt`,
  }

  serverProtocol = 'https'
}

/**
 * Open in browser (or don’t).
 */
const openInBrowser = env.LOCAL_BROWSER ? {
  // target: ["first.html", `http://localhost:8080/second.html`],
  app: {
    name: env.LOCAL_BROWSER,
  },
} : false

const isLocalhost = env.LOCAL_HOST == ('localhost' || '127.0.0.1')

/**
 * webpack-dev-server settings
 *
 * https://github.com/webpack/webpack-dev-server/blob/master/migration-v4.md
 */
configCSS.devServer = {
  host: env.LOCAL_HOST,
  port: isLocalhost ? 3000 : 'auto', // maybe should always be 'auto'…
  server: serverProtocol,
  https: devServerHttps,
  proxy: {
    '*': {
      target: env.LOCAL_URL,
      secure: false,
    },
  },
  devMiddleware: {
    publicPath: thePath('public'),
    writeToDisk: true,
  },
  hot: true,
  client: {
    logging: 'error',
    overlay: false, // wait for https://github.com/webpack/webpack-dev-server/issues/3689
    // overlay: {
    //     errors: true,
    //     warnings: false,
    // },
    progress: true,
  },
  open: openInBrowser,
  watchFiles: [ // other files than assets
    'app/**/*.php',
    'resources/**/*.php',
  ],
}

module.exports = [configCSS, configJs]
