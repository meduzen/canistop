const SW_VERSION = '1.4.6' // update it when the app changes
const DATA_CACHE_VERSION = '1.5' // update it when data structure changes

const suffix = 'DisplayNames' in Intl ? '-light' : ''

const resourcesCacheKey = `cache-v${SW_VERSION}`
const dataCacheKey = `data-v${DATA_CACHE_VERSION}`

const dataToCache = [
  `storage/metadata${suffix}.json`,
  'storage/data/ie-11.json', // default browser+version
]

const resourcesToCache = [
  '/',
  'site.webmanifest',

  'css/app.css',
  'js/app.js',

  // Standard icons
  'android-chrome-192x192.png',
  'android-chrome-512x512.png',
  'android-chrome-maskable-192x192.png',
  'android-chrome-maskable-512x512.png',
  'favicon.svg',

  // Apple icons
  'apple-touch-icon.png',
  'safari-pinned-tab.svg',

  // Microsoft icons tiles
  'browserconfig.xml',
  'mstile-70x70.png',
  'mstile-144x144.png',
  'mstile-150x150.png',
  'mstile-310x150.png',
  'mstile-310x310.png',

  // Old stuff
  'favicon-32x32.png',
  'favicon-16x16.png',
]

const createCaches = () => Promise.all([
  caches.open(resourcesCacheKey).then(cache => cache.addAll(resourcesToCache)),
  caches.open(dataCacheKey).then(cache => cache.addAll(dataToCache)),
])

const flushOldCaches = () => caches.keys().then(keys => Promise.all(
  keys
    .filter(key => key != dataCacheKey && key != resourcesCacheKey)
    .map(key => caches.delete(key).then(() => {

      // App has been updated
      if (key.startsWith('cache-v')) {
        notifyClients({ appUpdate: true })
      }
    }))
))

const putToCache = (cacheKey, request, response) =>
  caches.open(cacheKey).then(cache => cache.put(request, response))

const respondWith = (e, url) =>
  e.respondWith(caches.match(url, { ignoreSearch: true })
    .then(response => response || fetch(e.request).then(response => response))
  )

const notifyClients = data => self.clients.matchAll().then(clients =>
  clients.forEach(client => client.postMessage(data))
)

self.addEventListener('install', e =>
  e.waitUntil(createCaches().then(() => self.skipWaiting()))
)

self.addEventListener('activate', e =>
  e.waitUntil(flushOldCaches().then(() => self.clients.claim()))
)

self.addEventListener('fetch', e => {

  // Serve same root for `/` and `/b/`
  let url = new URL(e.request.url)
  if (url.pathname.startsWith('/b/')) {
    return respondWith(e, '/')
  }

  // Return from cache or fallback to network.
  respondWith(e, e.request)

  // Always fetch and re-cache (potentially new) data.
  if (e.request.url.includes('/storage/')) {
    fetch(e.request).then(response => {
      if (response.status == 200) {

        // Overwrite app data in cache.
        putToCache(dataCacheKey, e.request.clone(), response.clone())

        // Send data date to app.
        response.json().then(({ date }) => notifyClients({ date }))
      }
    })
  }
})

self.addEventListener('message', ({ data }) => {
  if (!('action' in data)) { return }

  if (data.action == 'updateDataCache') {
    caches.open(dataCacheKey)
      .then(cache => cache.keys().then(keys =>
        cache.addAll(keys.filter(key => key.url)))
      )
      .then(() => notifyClients({ dataCacheUpdate: true }))
  }

  if (data.action == 'requestCachedBrowsers') {
    caches.open(dataCacheKey).then(cache => cache.keys().then(keys =>
      notifyClients({
        cachedBrowsers: keys
          .map(key => key.url)
          .filter(url => url.includes('/storage/data/'))
      }))
    )
  }
})
