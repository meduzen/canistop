const state = {
  aboutIsVisible: false,
}

const mutations = {
  setAboutVisibility (state, value) {
    state.aboutIsVisible = value
  },
}

const actions = {
  toggleAboutVisibility ({ dispatch, commit }, value = !state.aboutIsVisible) {
    commit('setAboutVisibility', value)

    document.body.classList.toggle('body--about', value)

    if (value) {
      document.addEventListener('keydown', e => dispatch('handleEscapeKey', e))
    } else {
      document.removeEventListener('keydown', e => dispatch('handleEscapeKey', e))
    }
  },

  handleEscapeKey ({ dispatch }, { keyCode }) {
    if (keyCode == 27 && state.aboutIsVisible) {
      dispatch('toggleAboutVisibility', false)
    }
  },
}

export default { state, mutations, actions }
