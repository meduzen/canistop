<svg viewBox="0 0 115 110" width="18">
    <defs>
        <path id="star-path" stroke-width="20" d="M57.5 0L39.9 36.4 0 42l29 28-7 40 35.5-19 35.6 19L86 70l29-28-40-5.5L57.6 0z"/>
        <clipPath id="star-clip"><use xlink:href="#star-path"/></clipPath>
    </defs>
</svg>
