<svg id="filter-path" viewBox="0 0 25 21" stroke="currentColor" stroke-width="2.94" stroke-linecap="round">
    <line x1="1.74" y1="3.68" x2="23.4" y2="3.68"/>
    <line x1="1.74" y1="10.54" x2="23.4" y2="10.54"/>
    <line x1="1.74" y1="17.4" x2="23.4" y2="17.4"/>
    <line x1="14.04" y1="1.47" x2="14.04" y2="5.39"/>
    <line x1="7.89" y1="8.33" x2="7.89" y2="12.26"/>
    <line x1="18.65" y1="15.2" x2="18.65" y2="19.12"/>
</svg>
