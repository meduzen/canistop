<svg id="arrow-path" viewBox="0 0 11 15">
    <path fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M5.45 13.5V1.12m0 0L1.5 5.5m3.95-4.38L9.5 5.5"/>
</svg>
