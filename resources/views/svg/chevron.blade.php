<svg id="chevron-path" viewBox="0 0 16 9">
    <path d="M15 1L8 8 1 1" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>
