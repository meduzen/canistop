<meta property="og:site_name" content="Can I Stop">
<meta property="og:title" content="Can I Stop">
<meta property="og:description" content="A handy tool to decide if you can drop Internet Explorer 11 support.">
<meta property="og:url" content="{{ URL::to('/') }}">
<meta property="og:locale" content="en_US">
<meta property="og:image" content="{{ asset('/share-img-1800-945.jpg') }}">
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="1800">
<meta property="og:image:height" content="945">
<meta property="og:image:alt" content="A screen capture of the Can I Stop app showing some March 2019 data">

<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@canistopie11">
<meta name="twitter:creator" content="@meduzen">
