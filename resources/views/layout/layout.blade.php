<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        {{-- Preload right metadata depending on browser capabilities --}}
        <script>
            const suffix = 'DisplayNames' in Intl ? '-light' : ''
            fetch(`{{ Storage::url('') }}metadata${suffix}.json`)
        </script>

        @include('scripts.idb-detection')

        <title>Can I Stop</title>
        <meta name="description" content="Free yourself from Internet Explorer 11 in places where (almost) nobody uses it.">
        <meta name="keywords" content="browser, support, data, usage, ie, internet explorer, edge, front-end development">

        <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
        <meta name="supported-color-schemes" content="dark light only"> {{-- macOS 10.14.4 compat --}}
        <meta name="color-scheme" content="dark light only">
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}">

        @include('layout.manifest')
        @include('layout.sharing')

        <script src="{{ asset('/js/app.js') }}" defer></script>

        {{-- Google Search Console --}}
        <meta name="google-site-verification" content="kqLunfttyOl3O-DIo2vLgfjViPCk9iN4-Qm7ZFd5xuU" />
    </head>

    <body>
        @yield('app')

        {{-- Tracking --}}
        @if (config('matomo.server'))
        <img class="visually-hidden" src="{{ config('matomo.server') }}/piwik.php?idsite={{ config('matomo.site_id') }}&amp;rec=1" style="border:0" alt="" />
        @endif
    </body>
</html>
