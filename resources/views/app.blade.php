@extends('layout.layout')

@section('app')

{{-- App --}}
<div id="app">
    <noscript>
        <h1>Can I Stop</h1>
        <p>Free yourself from <strong>Internet Explorer 11</strong> in places where (almost) nobody uses it.</p>
        <p><small><em>Can I Stop</em> doesn’t work on Internet Explorer 11 (on purpose) and requires JavaScript.</small></p>
    </noscript>
</div>

{{-- SVG spritesheet --}}
<div class="visually-hidden">
    @include('svg.arrow')
    @include('svg.chevron')
    @include('svg.cross')
    @include('svg.filter')
    @include('svg.star')
    @include('svg.sync-arrow')
</div>

@endsection
